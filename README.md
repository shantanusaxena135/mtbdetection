This is a project space for the USU CS HCD group specifically for the mobile application to detect Brain Concussion. This will be used in the open study going on Sensory Motor Behavior Lab in HPER department of USU.  

This group works under the direction of Dr. Amanda Hughes in the Utah State University Computer Science department.

Copyrights reserved.